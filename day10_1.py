f = open('day10_input.txt')

bots = [[] for i in range(210)]
bot_inst = [[] for i in range(210)]
outputs = [-1 for i in range(21)]
bots_to_run = []

for line in f:
    line = line[:-1].split(' ')
    if line[0] == 'value':
        bots[int(line[-1])].append(int(line[1]))
        if len(bots[int(line[-1])]) == 2: bots_to_run.append(int(line[-1]))
    else:
        bot_inst[int(line[1])] = [(line[5], int(line[6])), (line[-2], int(line[-1]))]
while True:
    try:
        bot_index = bots_to_run.pop()
        bot = bots[bot_index]
        low, high = (bot[0], bot[1]) if bot[0] < bot[1] else (bot[1], bot[0])
        if low == 17 and high == 61:
            print(bot_index)
        low_inst, high_inst = bot_inst[bot_index]
        if low_inst[0] == 'output':
            outputs[low_inst[1]] = low
        else:
            bots[low_inst[1]].append(low)
            if len(bots[low_inst[1]]) == 2:
                bots_to_run.append(low_inst[1])
        if high_inst[0] == 'output':
            outputs[high_inst[1]] = high
        else:
            bots[high_inst[1]].append(high)
            if len(bots[high_inst[1]]) == 2:
                bots_to_run.append(high_inst[1])
    except IndexError as e:
        break
print(outputs)
