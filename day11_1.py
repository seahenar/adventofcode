from collections import defaultdict
from itertools import combinations
import time

# Ignores validity of path to be monotonic
def dist_heuristic(a, b):
    total = 0
    a_components = [0]*num_components
    b_components = [0]*num_components
    for i in range(1,5):
        for j in range(num_components):
            if a[i][j] == 1: a_components[j] = i
            if b[i][j] == 1: b_components[j] = i
    a_components.sort(reverse=True)
    b_components.sort(reverse=True)
    a1,a2 = a_components.pop(), a_components.pop()
    total += 4-a1
    for a in a_components:
        total += 2*(4-a)
    return total
    #for i in range(num_components):
    #    total += abs(a_components[i]-b_components[i])
    # Total floor distance
    #return abs(2*(total / 2)-3)

def is_valid_config(node):
    for i in range(1,5):
        generators = node[i][::2]
        num_generators = sum(generators)
        chips = node[i][1::2]
        if num_generators > 0:
            for i in range(len(chips)):
                if chips[i] == 1 and generators[i] == 0:
                    return False
    return True

def is_valid_config_up(node):
    if sum(map(sum, node[node[0]:])) <= 1:
        return False
    return True

def is_valid_config_dn(node):
    if sum(map(sum, node[1:node[0]+1])) <= 1:
        return False
    return True


def get_neighbours(node):
    e = node[0]
    e_floor = node[e]
    components_on_floor = []
    for i in range(num_components):
        if e_floor[i] == 1: components_on_floor.append(i)
    pairs = combinations(components_on_floor, 2)
    pairings = []
    for p in pairs:
        pairings.append(p)
    singles = combinations(components_on_floor, 1)
    for s in singles:
        pairings.append(s)
    neighbours = []
    for p in pairings:
        if e < 4:
            neighbour = list(node)
            for i in range(1,5):
                neighbour[i] = list(node[i])
            neighbour[0] += 1
            for p_i in p:
                neighbour[e][p_i] = 0
                neighbour[e+1][p_i] = 1
            if is_valid_config(neighbour) and is_valid_config_up(neighbour): neighbours.append(neighbour)
        if e > 1:
            neighbour = list(node)
            for i in range(1,5):
                neighbour[i] = list(node[i])
            neighbour[0] -= 1
            for p_i in p:
                neighbour[e][p_i] = 0
                neighbour[e-1][p_i] = 1
            if is_valid_config(neighbour) and is_valid_config_dn(neighbour): neighbours.append(neighbour)
    return neighbours

def make_path(cameFrom, current):
    total_path = [current]
    while current in cameFrom.keys():
        current = cameFrom[current]
        total_path.append(current)
    print("Length of path is: %d" % (len(total_path)-1))
    return total_path

def node_to_tuple(node):
    e = node[0]
    floors = [item for sublist in node[1:] for item in sublist]
    return tuple([e] + floors)

def tuple_to_node(tpl):
    e = tpl[0]
    floors = []
    for i in range(4):
        floors.append(list(tpl[i*num_components+1:(i+1)*num_components+1]))
    return ([e] + floors)

def a_star(start, goal):
    closedSet = set()
    openSet = set()
    start_tuple = node_to_tuple(start)
    goal_tuple = node_to_tuple(goal)
    openSet.add(start_tuple)
    cameFrom = {}

    gScore = defaultdict(lambda: 9999999)
    gScore[start_tuple] = 0

    fScore = defaultdict(lambda: 9999999)
    fScore[start_tuple] = dist_heuristic(start, goal)

    while openSet:
        lowest = 9999999
        for node_tuple in openSet:
            if fScore[node_tuple] < lowest:
                current_tuple = node_tuple
                lowest = fScore[node_tuple]
        current_node = tuple_to_node(current_tuple)

        if current_tuple == goal_tuple:
            return make_path(cameFrom, current_tuple)

        openSet.remove(current_tuple)
        closedSet.add(current_tuple)
        neighbours = get_neighbours(current_node)
        for n in neighbours:
            n_tuple = node_to_tuple(n)
            if n_tuple in closedSet:
                continue
            tentative_gScore = gScore[current_tuple] + 1
            if tentative_gScore + dist_heuristic(n, goal) > 35:
                continue
            if n_tuple not in openSet:
                openSet.add(n_tuple)
            elif tentative_gScore >= gScore[n_tuple]:
                continue

            cameFrom[n_tuple] = current_tuple
            gScore[n_tuple] = tentative_gScore
            fScore[n_tuple] = gScore[n_tuple] + dist_heuristic(n, goal)

#       [e, [prG, prM, coG, coM, cuG, cuM, ruG, ruM, plG, plM], ...]
start = [1, [1,1,0,0,0,0,0,0,0,0], [0,0,1,0,1,0,1,0,1,0], [0,0,0,1,0,1,0,1,0,1], [0,0,0,0,0,0,0,0,0,0]]
goal = [4, [0,0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0], [1,1,1,1,1,1,1,1,1,1]]
start_2 = [1, [1,1,0,0,0,0,0,0,0,0,1,1,1,1], [0,0,1,0,1,0,1,0,1,0,0,0,0,0], [0,0,0,1,0,1,0,1,0,1,0,0,0,0], [0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
goal_2 = [4, [0,0,0,0,0,0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0,0,0,0,0], [1,1,1,1,1,1,1,1,1,1,1,1,1,1]]
start_3 = [1, [1,1,0,0,0,0,0,0,0,0], [0,0,1,0,1,0,1,0,1,0], [0,0,0,1,0,1,0,1,0,1], [0,0,0,0,0,0,0,0,0,0]]
goal_3 = [4, [0,0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0,0,0], [1,1,1,1,1,1,1,1,1,1]]
global num_components
num_components = len(start_3[1])
t1 = time.clock()
a_star(start_3, goal_3)
t2 = time.clock()
print("%2f" % (t2-t1))
