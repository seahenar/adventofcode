from collections import defaultdict
from collections import Counter
from itertools import combinations
from itertools import chain
import time

def to_list(a):
    return list(chain.from_iterable(a))

def is_same(a, b):
    if a[0] != b[0]:
        return False
    else:
        a_z = zip(a[1::2],a[2::2])
        b_z = zip(b[1::2],b[2::2])
        a_z.sort()
        b_z.sort()
        if a_z != b_z:
            return False
    return True

# Ignores validity of path to be monotonic
def dist_heuristic(a, b):
    total = 0
    a_components = list(chain.from_iterable(a[1:]))
    b_components = list(chain.from_iterable(b[1:]))
    a_components.sort(reverse=True)
    b_components.sort(reverse=True)
    a1,a2 = a_components.pop(), a_components.pop()
    total += 4-a1
    for a in a_components:
        total += 2*(4-a)
    return total

def is_valid_config(node):
    num_generators = Counter(i[0] for i in node[1:])
    for g,c in node[1:]:
        if c != g and num_generators[c] > 0:
            return False
    return True

def is_valid_config_up(node):
    comps = Counter(list(chain.from_iterable(node[1:])))
    if sum([comps[i] for i in range(node[0],5)]) <= 1:
        return False
    return True

def is_valid_config_dn(node):
    comps = Counter(list(chain.from_iterable(node[1:])))
    if sum([comps[i] for i in range(1,node[0]+1)]) <= 1:
        return False
    return True


def get_neighbours(node):
    e = node[0]
    node = [node[0]]+list(chain.from_iterable(node[1:]))
    components_on_floor = [i for i in range(1,len(node)) if node[i] == e]
    pairs = combinations(components_on_floor, 2)
    pairings = []
    for p in pairs:
        pairings.append(p)
    singles = combinations(components_on_floor, 1)
    for s in singles:
        pairings.append(s)
    neighbours = set()
    for p in pairings:
        if e < 4:
            neighbour = list(node)
            neighbour[0] += 1
            for p_i in p:
                neighbour[p_i] += 1
            neighbour = [neighbour[0]]+zip(neighbour[1::2],neighbour[2::2])
            if is_valid_config(neighbour) and is_valid_config_up(neighbour):
                neighbour.sort()
                neighbours.add(tuple(neighbour))
        if e > 1:
            neighbour = list(node)
            neighbour[0] -= 1
            for p_i in p:
                neighbour[p_i] -= 1
            neighbour = [neighbour[0]]+zip(neighbour[1::2],neighbour[2::2])
            if is_valid_config(neighbour) and is_valid_config_dn(neighbour):
                neighbour.sort()
                neighbours.add(tuple(neighbour))
    return neighbours

def make_path(cameFrom, current, neigh_time, node_time, dh_time):
    total_path = [current]
    while current in cameFrom.keys():
        current = cameFrom[current]
        total_path.append(current)
    print("Length of path is: %d" % (len(total_path)-1))
    print("Time spent finding neighbours: %.02f" % (sum(neigh_time)))
    print("Time spent selecting node: %.02f" % (sum(node_time)))
    print("Time spent calculating dh: %.02f" % (sum(dh_time)))
    return total_path

def a_star(start, goal):
    neigh_time = []
    node_sel_time = []
    dh_time = []
    closedSet = set()
    openSet = set()
    openSet.add(start)
    cameFrom = {}

    gScore = defaultdict(lambda: 9999999)
    gScore[start] = 0

    fScore = defaultdict(lambda: 9999999)
    fScore[start] = dist_heuristic(start, goal)

    while openSet:
        lowest = 9999999
        t1 = time.clock()
        for node in openSet:
            if fScore[node] < lowest:
                current = node
                lowest = fScore[node]
        t2 = time.clock()
        node_sel_time.append(t2-t1)

        if current == goal:
            return make_path(cameFrom, current, neigh_time, node_sel_time, dh_time)

        openSet.remove(current)
        closedSet.add(current)
        t1 = time.clock()
        neighbours = get_neighbours(current)
        t2 = time.clock()
        neigh_time.append(t2-t1)
        for n in neighbours:
            if n in closedSet:
                continue
            t1 = time.clock()
            dh = dist_heuristic(n,goal)
            t2 = time.clock()
            dh_time.append(t2-t1)
            tentative_gScore = gScore[current] + 1
            if tentative_gScore + dh > 60:
                continue
            if n not in openSet:
                openSet.add(n)
            elif tentative_gScore >= gScore[n]:
                continue

            cameFrom[n] = current
            gScore[n] = tentative_gScore
            fScore[n] = gScore[n] + dh

#       [e, [prG, prM, coG, coM, cuG, cuM, ruG, ruM, plG, plM], ...]
start = (1,(1,1),(2,3),(2,3),(2,3),(2,3))
goal = (4,(4,4),(4,4),(4,4),(4,4),(4,4))
start_old = [1,1,1,2,3,2,3,2,3,2,3]
goal_old = [4,4,4,4,4,4,4,4,4,4,4]
start_2 = (1,(1,1),(2,3),(2,3),(2,3),(2,3),(1,1),(1,1))
goal_2 = (4,(4,4),(4,4),(4,4),(4,4),(4,4),(4,4),(4,4))
num_components = 2*(len(start_2)-1)
t1 = time.clock()
a_star(start_2, goal_2)
t2 = time.clock()
print("%2f" % (t2-t1))
