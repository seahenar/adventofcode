f = open('day12_input.txt')

registers = {'a':0, 'b':0, 'c':1, 'd':0}

def perform_instruction(instruction, inst_pointer):
    instruction = instruction.split(' ')
    if instruction[0] == 'inc':
        registers[instruction[1]] += 1
    elif instruction[0] == 'dec':
        registers[instruction[1]] -= 1
    elif instruction[0] == 'cpy':
        try:
            registers[instruction[2]] = registers[instruction[1]]
        except KeyError as e:
            registers[instruction[2]] = int(instruction[1])
    else:
        try:
            if registers[instruction[1]] != 0:
                inst_pointer += int(instruction[2]) - 1
        except KeyError as e:
            if int(instruction[1]) != 0:
                inst_pointer += int(instruction[2]) - 1
    return inst_pointer

instructions = []
inst_pointer = 0
for line in f:
    instructions.append(line[:-1])
while(inst_pointer < len(instructions)):
    inst_pointer = perform_instruction(instructions[inst_pointer], inst_pointer)
    inst_pointer += 1
print(registers)
