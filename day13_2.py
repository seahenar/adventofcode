from collections import defaultdict
from collections import deque

maze = [[-1 for i in range(50)] for j in range(50)]

def manhattan_dist(a,b):
    return (abs(a[0]-b[0]) + abs(a[1]-b[1]))

def get_neighbours(maze, node):
    x,y = node[0], node[1]
    n = []
    if x > 0:
        if maze[x-1][y] == '.': n.append((x-1,y))
    if maze[x+1][y] == '.': n.append((x+1,y))
    if y > 0:
        if maze[x][y-1] == '.': n.append((x,y-1))
    if maze[x][y+1] == '.': n.append((x,y+1))
    return n

def make_path(cameFrom, current, maze):
    total_path = [current]
    maze[current[0]][current[1]] = 'O'
    while current in cameFrom.keys():
        current = cameFrom[current]
        total_path.append(current)
        maze[current[0]][current[1]] = 'O'
    for line in maze:
        print(' '.join(line))
    print("Length of path is: %d" % (len(total_path)-1))
    return total_path

def a_star(start, goal, maze):
    closedSet = set()
    openSet = set()
    openSet.add(start)
    cameFrom = {}

    gScore = defaultdict(lambda: 9999999)
    gScore[start] = 0

    fScore = defaultdict(lambda: 9999999)
    fScore[start] = manhattan_dist(start, goal)

    while openSet:
        lowest = 9999999
        for node in openSet:
            if fScore[node] < lowest:
                current = node
                lowest = fScore[node]

        if current == goal:
            return make_path(cameFrom, current, maze)

        openSet.remove(current)
        closedSet.add(current)
        neighbours = get_neighbours(maze, current)
        for n in neighbours:
            if n in closedSet:
                continue
            tentative_gScore = gScore[current] + 1
            if n not in openSet:
                openSet.add(n)
            elif tentative_gScore >= gScore[n]:
                continue

            cameFrom[n] = current
            gScore[n] = tentative_gScore
            fScore[n] = gScore[n] + manhattan_dist(n, goal)

def make_map(nodes, maze):
    for n in nodes:
        maze[n[0]][n[1]] = 'O'
    for line in maze:
        print(' '.join(line))
    print("Number of nodes: %d" % len(nodes))

def breadth_first(start, maze):
    closedSet = set()
    openSet = deque()
    openSet.append(start)
    cameFrom = {}

    gScore = defaultdict(lambda: 9999999)
    gScore[start] = 0

    while openSet:
        current = openSet.popleft()

        closedSet.add(current)
        neighbours = get_neighbours(maze, current)
        for n in neighbours:
            if n in closedSet:
                continue
            tentative_gScore = gScore[current] + 1
            if tentative_gScore > 50:
                continue
            if n not in openSet:
                openSet.append(n)
            elif tentative_gScore >= gScore[n]:
                continue

            cameFrom[n] = current
            gScore[n] = tentative_gScore
    make_map(gScore.keys(), maze)


for x in range(50):
    for y in range(50):
        val = (x+3)*x + (y+1)*y + 2*x*y + 1358
        bits = bin(val).lstrip('0b').count('1')
        maze[y][x] = '.' if bits % 2 == 0 else '#'
breadth_first((1,1), maze)
#maze[39][31] = '!'
#for line in maze:
#    print(' '.join(line))
