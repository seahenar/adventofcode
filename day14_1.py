import hashlib

def check_for_triples(string):
    for i in range(len(string)-2):
        if string[i] == string[i+1] and string[i] == string[i+2]:
            return (string[i]*5)
    return None


def check_for_pents(string, patterns, index):
    indexes = []
    to_remove = None
    for p in patterns:
        if p[1]+1000 < index:
            to_remove = p
        else:
            if p[0] in string:
                indexes.append(p[1])
    if to_remove:
        patterns.remove(to_remove)
    indexes.sort()
    return indexes

search_for = set()

salt = 'ahsbgdzn'
index = 0

keys = []

while len(keys) < 70:
    input_str = salt + str(index)
    m = hashlib.md5()
    m.update(input_str)
    output_str = m.hexdigest()
    pents = check_for_pents(output_str, search_for, index)
    if pents:
        for p in pents:
            keys.append(p)
        keys.sort()
    triple = check_for_triples(output_str)
    if triple:
        search_for.add((triple, index))
    index += 1
print(keys[:64])
