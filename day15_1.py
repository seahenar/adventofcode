discs = [(13,1), (19,10), (3,2), (7,1), (5,3), (17,5)]
discs = [(13,1), (19,10), (3,2), (7,1), (5,3), (17,5), (11,0)]

t = 7
found = False
while t < 10000000 and not found:
    n = 1
    for disc in discs:
        if not ((disc[1]+t+n) % disc[0] == 0):
            found = False
            break
        n += 1
        found = True
    t += 19
print(t-19)
