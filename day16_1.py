a = '00111101111101000'
limit_1 = 272
limit_2 = 35651584
limit = limit_2
# Dragon curve it
while len(a) < limit:
    b = a[::-1]
    b = b.replace('1','.')
    b = b.replace('0','1')
    b = b.replace('.','0')
    a = '0'.join((a,b))
# Find checksum
checksum = map(int, list(a)[:limit])
while len(checksum) % 2 == 0:
    checksum = [(x+y+1)%2 for (x,y) in zip(checksum[::2], checksum[1::2])]
checksum = ''.join(map(str, checksum))
print(checksum)
