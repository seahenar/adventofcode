from collections import defaultdict
from collections import deque
import hashlib

maze = [[-1 for i in range(50)] for j in range(50)]

def manhattan_dist(a,b):
    return (abs(a[0]-b[0]) + abs(a[1]-b[1]))

def get_neighbours(node):
    path = node[1]
    x,y = node[0]
    hash = hashlib.md5()
    hash.update(path)
    result = hash.hexdigest()[:4]
    open = "bcdef"
    neighbours = []
    if x > 1 and result[0] in open:
        neighbours.append(((x-1,y), path+'U'))
    if x < 4 and result[1] in open:
        neighbours.append(((x+1,y), path+'D'))
    if y > 1 and result[2] in open:
        neighbours.append(((x,y-1), path+'L'))
    if y < 4 and result[3] in open:
        neighbours.append(((x,y+1), path+'R'))
    return neighbours

def make_path(cameFrom, current, maze):
    total_path = [current]
    maze[current[0]][current[1]] = 'O'
    while current in cameFrom.keys():
        current = cameFrom[current]
        total_path.append(current)
        maze[current[0]][current[1]] = 'O'
    for line in maze:
        print(' '.join(line))
    print("Length of path is: %d" % (len(total_path)-1))
    return total_path

def breadth_first(start):
    openSet = deque()
    openSet.append(start)

    while openSet:
        current = openSet.popleft()

        if current[0] == (4,4):
            f = open('day17_output.txt','w')
            f.write(current[1][8:])
            print(current[1][8:])
            f.close()
            break

        neighbours = get_neighbours(current)
        for n in neighbours:
            openSet.append(n)

# Data Structure
# ((x, y), path)

breadth_first(((1,1), 'dmypynyp'))
