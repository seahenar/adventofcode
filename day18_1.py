from collections import defaultdict
from collections import deque
import hashlib

# If L!=R, then trapped. If L==R then safe. Center is irrelevant

def nextval(left, right):
    if (left+right) % 2 == 0:
        return 1
    return 0

# 1 = safe, 0 = trap
maze = [[1,1,1,1,1,1,0,1,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,1,1,1,0,1,0,1,1,0,0,1,0,0,0,1,1,0,1,0,1,1,0,1,0,0,0,1,0,0,0,0,1,1,0,0,1,0,1,0,1,1,1,1,1,0,0,0,0,0,1,1,0,1,1,0,0,0,1,1,0,0,1,0,1,0,1,1,0,0,1,1,0,0,0,1,1]]
rowlen = len(maze[0])

row = 0
while row < 399999:
    i = 1
    new = []
    new.append(nextval(1, maze[row][1]))
    while i < rowlen-1:
        new.append(nextval(maze[row][i-1], maze[row][i+1]))
        i += 1
    new.append(nextval(maze[row][-2], 1))
    maze.append(new)
    row += 1

total = 0
for rows in maze:
    total += sum(rows)
print(total)
