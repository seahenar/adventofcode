from collections import defaultdict
from collections import deque
import hashlib
import time

class Node:
    def __init__(self, cargo=None, next=None):
        self.cargo = cargo
        self.next = next
    def __str__(self):
        return str(self.cargo)

# Alternate purging evens and odds
# Remove 2,4,6,...
# Remove 1,5,9,...
# Remove 3,11,19,...
#

start = time.clock()
nodes = range(1,3014388)
cur = 0
while(len(nodes)>1):
    if len(nodes)%2 == 0:
        nxt = 0
    else:
        nxt = 1
    nodes = nodes[cur::2]
    cur = (cur+nxt)%2
print(nodes)
end = time.clock()
print("%.2f" % (end-start))
