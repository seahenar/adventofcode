from collections import defaultdict
from collections import deque
from math import ceil
import hashlib
import time

# Remove 2, skip 1.

class Node:
    def __init__(self, cargo=None, next=None):
        self.cargo = cargo
        self.next = next
    def __str__(self):
        return str(self.cargo)

start = time.clock()
nodes = []
first = Node(1)
nodes.append(first)
prev = first
top = 3014387
mid = int(ceil(top/2))
for i in range(2,top):
    nxt = Node(i)
    nodes.append(nxt)
    prev.next = nxt
    prev = nxt
last = Node(top)
nodes.append(last)
prev.next = last
last.next = first
nodes[mid-1].next = nodes[mid+1]
cur = nodes[mid+1]
while(True):
    cur.next = cur.next.next.next
    cur = cur.next
    if cur.next.next == cur:
        print(cur)
        break
end = time.clock()
print("%.2f" % (end-start))
