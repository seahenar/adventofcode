input = "L2, L3, L3, L4, R1, R2, L3, R3, R3, L1, L3, R2, R3, L3, R4, R3, R3, L1, L4, R4, L2, R5, R1, L5, R1, R3, L5, R2, L2, R2, R1, L1, L3, L3, R4, R5, R4, L1, L189, L2, R2, L5, R5, R45, L3, R4, R77, L1, R1, R194, R2, L5, L3, L2, L1, R5, L3, L3, L5, L5, L5, R2, L1, L2, L3, R2, R5, R4, L2, R3, R5, L2, L2, R3, L3, L2, L1, L3, R5, R4, R3, R2, L1, R2, L5, R4, L5, L4, R4, L2, R5, L3, L2, R4, L1, L2, R2, R3, L2, L5, R1, R1, R3, R4, R1, R2, R4, R5, L3, L5, L3, L3, R5, R4, R1, L3, R1, L3, R3, R3, R3, L1, R3, R4, L5, L3, L1, L5, L4, R4, R1, L4, R3, R3, R5, R4, R3, R3, L1, L2, R1, L4, L4, L3, L4, L3, L5, R2, R4, L2"
input = input.split(', ')
found = False
visited = set((0,0))
pos = [0,0]
bearings = [(0,1), (1,0), (0,-1), (-1,0)]
bearing_name = ["N","E","S","W"]
bearing = 0
for c in input:
    dir = c[0]
    num = int(c[1:])
    if dir == "R":
        bearing = (bearing + 1) % 4
    else:
        bearing = (bearing - 1) % 4
    for i in range(num):
        pos[0] += bearings[bearing][0]
        pos[1] += bearings[bearing][1]
        h = pos[0]*10000 + pos[1]
        if h in visited:
            found = True
            break
        else:
            visited.add(h)
    print(pos, bearing_name[bearing], c)
    if found: break
print(pos)
print(abs(pos[0]) + abs(pos[1]))
