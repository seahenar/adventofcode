from collections import defaultdict
from collections import deque
from math import ceil
import hashlib
import time

currentLowest = 0
changed = 1
while(True):
    if changed == 0:
        print(currentLowest)
        break
    changed = 0
    f = open('day20_input.txt')
    for line in f:
        line = line[:-1].split("-")
        start = int(line[0])
        end = int(line[1])
        if start <= currentLowest and end > currentLowest:
            currentLowest = end+1
            changed = 1
