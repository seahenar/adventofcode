from collections import defaultdict
from collections import deque
from math import ceil
import hashlib
import time

def rotate(l, n):
    return l[-n:] + l[:-n]

# 1 = swap pX pY
# 2 = swap cX cY
# 3 = rotate D X
# 4 = rotate right letter X
# 5 = reverse X Y
# 6 = move X Y
def scramble(pw, inp):
    if inp[0] == 1:
        pw[inp[1]], pw[inp[2]] = pw[inp[2]], pw[inp[1]]
    elif inp[0] == 2:
        a,b = pw.index(inp[1]), pw.index(inp[2])
        pw[a], pw[b] = pw[b], pw[a]
    elif inp[0] == 3:
        if inp[1] == 'r':
            pw = rotate(pw, 0-inp[2])
        else:
            pw = rotate(pw, inp[2])
    elif inp[0] == 4:
        rs = 1
        i = pw.index(inp[1])
        if (i == 0):
            rs = 1 # 7->0 h
        elif (i == 1):
            rs = 1 # 0->1 b
        elif (i == 2):
            rs = 6 # 4->2 c
        elif (i == 3):
            rs = 2 # 1->3 g
        elif (i == 4):
            rs = 7 # 5->4 d
        elif (i == 5):
            rs = 3 # 2->5 f
        elif (i == 6):
            rs = 0 # 6->6 e
        elif (i == 7):
            rs = 4 # 3->7 a
        pw = rotate(pw, 0-rs)
    elif inp[0] == 5:
        pw[inp[1]:inp[2]+1] = pw[inp[1]:inp[2]+1][::-1]
    elif inp[0] == 6:
        val = pw.pop(inp[2])
        pw.insert(inp[1], val)
    return pw

f = open('day21_input.txt')

#pw = ['a','b','c','d','e','f','g','h']
pw = ['f','b','g','d','c','e','a','h']
for line in reversed(f.readlines()):
    inp = []
    print()
    print(line[:-1])
    line = line[:-1].split(" ")
    if line[0] == "swap" and line[1] == "letter":
        inp = [2, line[2], line[5]]
    elif line[0] == "swap" and line[1] == "position":
        inp = [1, int(line[2]), int(line[5])]
    elif line[0] == "rotate":
        if line[1] == "right" or line[1] == "left":
            inp = [3, line[1][0], int(line[2])]
        elif line[1] == "based":
            inp = [4, line[6]]
    elif line[0] == "reverse":
        inp = [5, int(line[2]), int(line[4])]
    elif line[0] == "move":
        inp = [6, int(line[2]), int(line[5])]
    oldpw = list(pw)
    pw = scramble(pw, inp)
    print("".join(oldpw) + " -> " + "".join(pw))
    #print(pw)
print(pw)
