from collections import defaultdict
from collections import deque
from math import ceil
import hashlib
import time

f = open('day22_input.txt')
used = dict()
avail = dict()

# List of used
# List of avail
f.readline()
f.readline()
for line in f:
    l = line[:-1].split()
    us = int(l[2][:-1])
    av = int(l[3][:-1])
    if us > 0:
        if us in used:
            used[us] += 1
        else:
            used[us] = 1
    if av in avail:
        avail[av] += 1
    else:
        avail[av] = 1
print(used)
print(avail)
