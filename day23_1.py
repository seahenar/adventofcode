f = open('day23_input.txt')

registers = {'a':7, 'b':0, 'c':0, 'd':0}

def perform_instruction(instruction, inst_pointer, instructions):
    instruction = instruction.split(' ')
    if instruction[0] == 'inc':
        registers[instruction[1]] += 1
    elif instruction[0] == 'tgl':
        print(registers)
        print(instructions)
        try:
            inst = instructions[inst_pointer+registers[instruction[1]]].split()
            if inst[0] == 'inc':
                inst[0] = 'dec'
            elif inst[0] == 'dec':
                inst[0] = 'inc'
            elif inst[0] == 'tgl':
                inst[0] = 'inc'
            elif inst[0] == 'cpy':
                inst[0] = 'jnz'
            elif inst[0] == 'jnz':
                inst[0] = 'cpy'
            inst = " ".join(inst)
            instructions[inst_pointer+registers[instruction[1]]] = inst
        except (KeyError, IndexError) as e:
            print()
    elif instruction[0] == 'dec':
        registers[instruction[1]] -= 1
    elif instruction[0] == 'cpy':
        try:
            registers[instruction[2]] = registers[instruction[1]]
        except KeyError as e:
            try:
                registers[instruction[2]] = int(instruction[1])
            except KeyError as e:
                print()
    else:
        try:
            if registers[instruction[1]] != 0:
                try:
                    inst_pointer += int(instruction[2]) - 1
                except ValueError as e:
                    inst_pointer += registers[instruction[2]] - 1
        except KeyError as e:
            if int(instruction[1]) != 0:
                try:
                    inst_pointer += int(instruction[2]) - 1
                except ValueError as e:
                    inst_pointer += registers[instruction[2]] - 1
    return instructions, inst_pointer

instructions = []
inst_pointer = 0
for line in f:
    instructions.append(line[:-1])
while(inst_pointer < len(instructions)):
    #print(registers)
    #print(inst_pointer)
    #print(instructions)
    #raw_input()
    instructions, inst_pointer = perform_instruction(instructions[inst_pointer], inst_pointer, instructions)
    inst_pointer += 1
print(registers)
