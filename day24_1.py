from collections import defaultdict
from collections import deque
from itertools import permutations
from math import ceil
import hashlib
import time

f = open('day24_input.txt')

# First part - find all pair wise distances between nodes.
# Second part - perform dijkstras to find shortest path between all nodes.

maze = [[-1 for i in range(179)] for j in range(39)]

def get_neighbours(maze, node):
    x,y = node[0], node[1]
    n = []
    if maze[x-1][y] == 0: n.append((x-1,y))
    if maze[x+1][y] == 0: n.append((x+1,y))
    if maze[x][y-1] == 0: n.append((x,y-1))
    if maze[x][y+1] == 0: n.append((x,y+1))
    return n

def breadth_first(start, maze, goals):
    closedSet = set()
    openSet = deque()
    openSet.append(start)
    found = 0
    found_distances = {}
    #cameFrom = {}

    gScore = defaultdict(lambda: 9999999)
    gScore[start] = 0

    while found < 7:
        current = openSet.popleft()

        closedSet.add(current)
        neighbours = get_neighbours(maze, current)
        for n in neighbours:
            if n in closedSet:
                continue
            tentative_gScore = gScore[current] + 1
            if n not in openSet:
                openSet.append(n)
                if n in goals:
                    found += 1
                    found_distances[(goals.index((0,0)),goals.index(n))] = tentative_gScore
            gScore[n] = tentative_gScore
    return found_distances

goals = {0:(0,0), 1:(0,0), 2:(0,0), 3:(0,0), 4:(0,0), 5:(0,0), 6:(0,0), 7:(0,0)}
goal_set = set()
nodes = {}
nodes_inv = {}
edges = set()
num_nodes = 0

j = 0
for line in f:
    line = line[:-1]
    i = 0
    for c in line:
        if c == '#':
            maze[j][i] = -1
        elif c == '.':
            maze[j][i] = 0
            if maze[j-1][i] == 0:
                edges.add((num_nodes, nodes_inv[(j-1,i)]))
            if maze[j][i-1] == 0:
                edges.add((num_nodes, nodes_inv[(j,i-1)]))
            nodes[num_nodes] = (j,i)
            nodes_inv[(j,i)] = num_nodes
            num_nodes += 1
        else:
            maze[j][i] = 0
            if maze[j-1][i] == 0:
                edges.add((num_nodes, nodes_inv[(j-1,i)]))
            if maze[j][i-1] == 0:
                edges.add((num_nodes, nodes_inv[(j,i-1)]))
            nodes[num_nodes] = (j,i)
            nodes_inv[(j,i)] = num_nodes
            goals[int(c)] = (j,i)
            num_nodes += 1
        i += 1
    j += 1

print(goals)
distances = {}
for i in range(8):
    g = goals.values()
    g[i] = (0,0)
    ds = breadth_first(goals[i], maze, g)
    distances.update(ds)
min_path = 999999
for p in permutations([1,2,3,4,5,6,7]):
    path = distances[(0,p[0])]
    for n in range(6):
        path += distances[(p[n],p[n+1])]
    min_path = path if path < min_path else min_path
print(min_path)
