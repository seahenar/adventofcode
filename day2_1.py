f = open('day2_input.txt')

keypad = [[1,2,3],[4,5,6],[7,8,9]]

code = []
for line in f:
    val = [1,1]
    for c in line[:-1]:
        if c == 'U' and val[0] > 0:
            val[0] -= 1
        elif c == 'D' and val[0] < 2:
            val[0] += 1
        elif c == 'R' and val[1] < 2:
            val[1] += 1
        elif c == 'L' and val[1] > 0:
            val[1] -= 1
    code.append(keypad[val[0]][val[1]])
print(code)
