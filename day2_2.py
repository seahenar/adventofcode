f = open('day2_input.txt')

keypad = [[0,0,1,0,0],[0,2,3,4,0],[5,6,7,8,9],[0,'A','B','C',0],[0,0,'D',0,0]]

code = []
for line in f:
    val = [2,0]
    for c in line[:-1]:
        if c == 'U' and val[0] > 0:
            val[0] = (val[0] - 1) if (keypad[val[0]-1][val[1]]) != 0 else val[0]
        elif c == 'D' and val[0] < 4:
            val[0] = (val[0] + 1) if (keypad[val[0]+1][val[1]]) != 0 else val[0]
        elif c == 'R' and val[1] < 4:
            val[1] = (val[1] + 1) if (keypad[val[0]][val[1]+1]) != 0 else val[1]
        elif c == 'L' and val[1] > 0:
            val[1] = (val[1] - 1) if (keypad[val[0]][val[1]-1]) != 0 else val[1]
    code.append(keypad[val[0]][val[1]])
print(code)
