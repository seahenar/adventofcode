f = open('day3_input.txt')

def is_valid(a,b,c):
    if (a+b)>c and (a+c)>b and (b+c)>a:
        return True
    else:
        return False

txt = []

num = 0
for line in f:
    l = line[:-1].split()
    if is_valid(int(l[0]),int(l[1]),int(l[2])): num += 1
print(num)
