f = open('day3_input.txt')

def is_valid(a,b,c):
    if (a+b)>c and (a+c)>b and (b+c)>a:
        return True
    else:
        return False

txt = []

num = 0
for line in f:
    txt.append([int(x) for x in line[:-1].split()])
i = 0
while(i < len(txt)):
    for j in [0,1,2]:
        if is_valid(txt[i][j],txt[i+1][j],txt[i+2][j]): num += 1
    i += 3
print(num)
