from collections import Counter
import operator

f = open('day4_input.txt')

def is_valid(text, checksum):
    counts = Counter(text).most_common()
    counts = sorted(sorted(counts, key=operator.itemgetter(0)), key=operator.itemgetter(1), reverse=True)
    for i in range(5):
        if counts[i][0] != checksum[i]: return False
    return True

num = 0
for line in f:
    l = line[:-1].split('-')
    txt = ''.join(l[:-1])
    val = l[-1][:-7]
    checksum = l[-1][-6:-1]
    if is_valid(txt, checksum): num += int(val)
print(num)
