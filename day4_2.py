from collections import Counter
import operator

f = open('day4_input.txt')

def is_valid(text, checksum):
    counts = Counter(text).most_common()
    counts = sorted(sorted(counts, key=operator.itemgetter(0)), key=operator.itemgetter(1), reverse=True)
    for i in range(5):
        if counts[i][0] != checksum[i]: return False
    return True

def decrypt(text, shift):
    shift = shift % 26
    new_text = []
    for c in text:
        if c != ' ':
            #new_c = ord(c) += shift
            #if new_c > ord('z'): new_c -= 26
            #new_c = chr(new_c)
            new_c = chr(ord(c) + shift if ord(c) + shift <= ord('z') else ord(c) + shift - 26)
        else: new_c = c
        new_text.append(new_c)
    return ''.join(new_text)



for line in f:
    l = line[:-1].split('-')
    txt = ''.join(l[:-1])
    val = l[-1][:-7]
    checksum = l[-1][-6:-1]
    if is_valid(txt, checksum):
        dec_text = decrypt(' '.join(l[:-1]), int(val))
        if 'north pole' in dec_text or 'northpole' in dec_text:
            print(dec_text, val)
            #break
