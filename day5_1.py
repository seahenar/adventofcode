import hashlib

inp = "wtnhxymk"
i = 0
code = []
while len(code) < 8:
    m = hashlib.md5()
    m.update((inp + str(i)).encode('utf-8'))
    digest = m.hexdigest()
    if digest[0:5] == '00000':
        code.append(digest[5])
        print(code)
    i += 1
print(''.join(code))
