import hashlib

inp = "wtnhxymk"
i = 0
found = 0
code = [0,0,0,0,0,0,0,0]
while found < 8:
    m = hashlib.md5()
    m.update((inp + str(i)).encode('utf-8'))
    digest = m.hexdigest()
    if digest[0:5] == '00000':
        pos = digest[5]
        if ord(pos) >= ord('0') and ord(pos) <= ord('7'):
            if code[int(pos)] == 0:
                found += 1
                code[int(pos)] = digest[6]
                print(code)
    i += 1
print(''.join(code))
