from collections import Counter

f = open('day6_input.txt')

lines = ['','','','','','','','']
for line in f:
    for i in range(8):
        lines[i] += line[i]
output = ''
for i in range(8):
    counts = Counter(lines[i]).most_common(3)
    print(counts)
    output += counts[0][0]
print(output)
