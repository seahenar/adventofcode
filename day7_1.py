from collections import Counter

f = open('day7_input.txt')

def contains_abba(seq):
    for string in seq:
        for i in range(len(string)-4):
            if string[i] == string[i+3] and string[i+1] == string[i+2]: return True
    return False

total = 0
for line in f:
    s = line[:-1]
    num_brackets = s.count('[')
    s = s.replace('[','.')
    s = s.replace(']','.')
    s = s.split('.')
    hyper_seq = []
    normal_ip = []
    for i in range(num_brackets):
        normal_ip.append(s[2*i])
        hyper_seq.append(s[2*i+1])
    normal_ip.append(s[-1])
    if contains_abba(normal_ip) and not contains_abba(hyper_seq): total += 1
print(total)
