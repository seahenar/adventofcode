from collections import Counter

f = open('day7_input.txt')

def contains_aba(aba_seq, bab_seq):
    babs = []
    for string in aba_seq:
        for i in range(len(string)-2):
            if string[i] == string[i+2] and string[i] != string[i+1]:
                babs.append(string[i+1]+string[i]+string[i+1])
    for string in bab_seq:
        for bab in babs:
            if bab in string: return True
    return False


total = 0
for line in f:
    s = line[:-1]
    num_brackets = s.count('[')
    s = s.replace('[','.')
    s = s.replace(']','.')
    s = s.split('.')
    hyper_seq = []
    normal_ip = []
    for i in range(num_brackets):
        normal_ip.append(s[2*i])
        hyper_seq.append(s[2*i+1])
    normal_ip.append(s[-1])
    if contains_aba(normal_ip, hyper_seq): total += 1
print(total)
