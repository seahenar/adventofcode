from collections import Counter

f = open('day8_input.txt')

pixels = [[0 for i in range(50)] for i in range(6)]
for line in f:
    line = line[:-1].split(' ')
    if 'rect' in line[0]:
        y,x = line[1].split('x')
        y = int(y)
        x = int(x)
        for i in range(x):
            for j in range(y):
                pixels[i][j] = 1
    else:
        if 'column' in line[1]:
            x = int(line[2].split('=')[1])
            val = int(line[4])
            new_pixels = [0 for i in range(6)]
            for i in range(6):
                new_pixels[i] = pixels[i-val][x]
            for i in range(6):
                pixels[i][x] = new_pixels[i]
        else:
            y = int(line[2].split('=')[1])
            val = int(line[4])
            new_pixels = [0 for i in range(50)]
            for i in range(50):
                new_pixels[i] = pixels[y][i-val]
            for i in range(50):
                pixels[y][i] = new_pixels[i]
total = 0
for i in range(len(pixels)):
    print(pixels[i])
    total += sum(pixels[i])
print(total)
