f = open('day9_input.txt')

def decompress(text):
    text_len = len(text)
    total = 0
    i = 0
    while i < text_len:
        if text[i] == '(':
            i += 1
            num_chars = 0
            while text[i] != 'x':
                num_chars = num_chars*10
                num_chars += int(text[i])
                i += 1
            num_repeat = 0
            i += 1
            while text[i] != ')':
                num_repeat = num_repeat*10
                num_repeat += int(text[i])
                i += 1
            i += 1
            if i + num_chars <= text_len:
                total += num_repeat * decompress(text[i:i+num_chars])
            else:
                total += num_repeat * decompress(text[i:-1])
                break
            i += num_chars
        else:
            total += 1
            i += 1
    return total

text = []
for line in f:
    text += list(line[:-1])
text_len = len(text)
total = decompress(text)
print(total)
